<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Seos {
    
    
    //const seo_data_key_description = 'seo_description';
        const seo_data_key_image = 'seo_image';
       // const seo_data_key_keywords = 'seo_keywords';
       // const seo_data_ARRAY = 'SEOS';
         const seo_data_title = 'SEO_title';
    
    
    
        const fbpage = '';
        const googlePlus = '';
        const linkedin = '';
        const twitter = '';
        
        const apptitle = "PACESPrep";
        
        // image_name
        const image_common1 = 'medical-courses-india';
        const image_common2 = 'mrcp training';
        const image_common3 = 'mrcp training in kerala';
        const image_common4 = 'mrcp training in india';
        const image_common5 = 'mrcp training in aster mims';
        
        
        
        // main page
        const seo_key_1 = 'mrcp (UK)';
        const seo_key_2 = 'Aster mims courses';
        const seo_key_3 = 'mrcp';
        const seo_key_4 = 'MRCP PACES Courses in India';
        const seo_key_5 = 'Aster mims';
        const seo_key_6 = 'Kottakkal';
        const seo_key_7 = 'kottakkal arya vaidya sala';
        const seo_key_8 = 'medical ';
        const seo_key_9 = 'medical courses';
        const seo_key_10 = 'kerala Aster mims';
        
        
        const seo_sentance1 = 'Aster mims group';
        const seo_sentance2 = 'PACESPrep';
        const seo_sentance3 = 'mrcp (uk)';
          
        
        // sub pages
         
}
