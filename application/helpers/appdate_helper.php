<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



if (!function_exists('getDayCountInBetween')) {

    function getDayCountInBetween($date1, $date2) {
        $d2 = strtotime($date2);
        $d1 = strtotime($date1);
        $datediff = $d2 - $d1;
        $dayCount = floor($datediff / (60 * 60 * 24));
        return $dayCount;
    }

}

if (!function_exists('getFirstDayOfMonth')) {

    function getFirstDayOfMonth($date = null) {
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        $day = null;
        if ($date != null) {
            //date("Y-m-t", strtotime($day));
        } else {
            $day = date('Y-m-01 00:00:01');
        }
        return $day;
    }

}

if (!function_exists('getLastDayOfMonth')) {

    function getLastDayOfMonth($date = null) {
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        $day = null;
        if ($date != null) {
            
        } else {
            $day = date('Y-m-t 23:59:59');
        }
        return date("Y-m-t 23:59:59", strtotime($day));
    }

}
if (!function_exists('appdateformat')) {

    function appdateformat($originalDate) {
        return date("d-m-Y H:i:s", strtotime($originalDate));
    }

}

if (!function_exists('getCurrentDateWithTime')) {

    function getCurrentDateWithTime() {
        
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        return date('Y-m-d H:i:s');
    }

}
if (!function_exists('getCurrentDate')) {

    function getCurrentDate() {
        
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        return date('Y-m-d');
    }

}
if (!function_exists('getCurrentHR_MIN')) {

    function getCurrentHR_MIN() {
        // date_default_timezone_set('Asia/Kolkata');
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        return date(' H:i:s');
    }

}
if (!function_exists('getCurrentDateWithDayName')) {

    function getCurrentDateWithDayName() {
        //  date_default_timezone_set('Asia/Kolkata');
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        return date('l.d-m-Y');
    }

}
if (!function_exists('getCurrentDay')) {

    function getCurrentDay() {
        
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        return date('l');
    }

}

if (!function_exists('getDayFromDate')) {

    function getDayFromDate($date) {
        
        date_default_timezone_set(Cons::APP_TIME_ZONE);
        
        return date('l',strtotime($date));
    }

}


if (!function_exists('appDateFormat')) {

    function appDateFormat1($originalDate) {
        return date("d-m-Y H:i:s", strtotime($originalDate));
    }

}
if (!function_exists('addCurrentTimeToDate')) {

    function addCurrentTimeToDate($originalDate) {
        $currentdate = getCurrentDate();
        $currenttime = $currentdate->format('H:i:s');

        // return date("d-m-Y H:i:s", strtotime($originalDate));
    }

}

if (!function_exists('uiDate2DBdate')) {

    function uiDate2DBdate($originalDate, $isTimeAdded = false) {
        date_default_timezone_set('Asia/Dubai');
        if (isset($originalDate) && trim($originalDate) != '') {
            if ($isTimeAdded) {
                return date("Y-m-d H:i:s", strtotime($originalDate));
            } else {
                return date("Y-m-d", strtotime($originalDate));
            }
        } else {
            return '';
        }
    }

}

if (!function_exists('dbDate2UIdate')) {

    function dbDate2UIdate($originalDate, $isTimeAdded = false) {
        date_default_timezone_set('Asia/Dubai');
        if (isset($originalDate) && trim($originalDate) != '') {
            if ($isTimeAdded) {
                return date("d-m-Y H:i:s", strtotime($originalDate));
            } else {
                return date("d-m-Y", strtotime($originalDate));
            }
        } else {
            return '';
        }
    }

}

/*
  if (!function_exists('appDateDDMMMMMMYYYY')) {

  function appDateDDMMMMMMYYYY($inputDate) {
  return date("d F Y", strtotime($inputDate));
  }

  }

  if (!function_exists('addDateDDMMMYY')) {

  function addDateDDMMMYY($inputDate) {
  return date("d-M-y", strtotime($inputDate));
  }

  } */

//---------------------------

if (!function_exists('dateStrToTimestap_start')) {

    function dateStrToTimestap_start() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }

}
if (!function_exists('convertStringtodate')) {

    function convertStringtodate($strDate) {

        $time = strtotime($strDate);

        $dateval = date('Y-m-d H:i:s', $time);
        return $dateval;
    }

}
if (!function_exists('dateStrToTimestap_end')) {

    function dateStrToTimestap_end() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }

}

if (!function_exists('currentdateMilsec')) {

    function currentdateMilsec() {
        $milliseconds = round(microtime(true) * 1000);
        return $milliseconds;
    }

}

if (!function_exists('convertStringtodate')) {

    function convertStringtodate($strDate) {
        $time = strtotime($strDate);

        $dateval = date('Y-m-d H:i:s', $time);
        return $dateval;
    }

}



