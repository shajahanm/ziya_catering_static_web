<html class="no-js"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= Cons::common_title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= Cons::common_title ?>" />
        <meta name="keywords" content="<?= Cons::common_title ?>" />
        <meta name="author" content="<?= Cons::common_title ?>" />


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('webres/public/images/new-favicon/apple-icon-57x57.png') ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('webres/public/images/new-favicon/apple-icon-60x60.png') ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('webres/public/images/new-favicon/apple-icon-72x72.png') ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('webres/public/images/new-favicon/apple-icon-76x76.png') ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('webres/public/images/new-favicon/apple-icon-114x114.png') ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('webres/public/images/new-favicon/apple-icon-120x120.png') ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('webres/public/images/new-favicon/apple-icon-144x144.png') ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('webres/public/images/new-favicon/apple-icon-152x152.png') ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('webres/public/images/new-favicon/apple-icon-180x180.png') ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('webres/public/images/new-favicon/android-icon-192x192.png') ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('webres/public/images/new-favicon/favicon-32x32.png') ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('webres/public/images/new-favicon/favicon-96x96.png') ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('webres/public/images/new-favicon/favicon-16x16.png') ?>">


		<link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/style.min.css?ver=5.2.4'  ?>">
		<link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/artnova_main.css' ?>">
    <?php  /* ?>   
    
      <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->
     <!-- Animate.css -->
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/animate.css' ?>">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/icomoon.css' ?>">
        <!-- Bootstrap Core Css -->
        <link href="<?php echo base_url() . 'webres/public/bootstrap/css/bootstrap.css' ?>" rel="stylesheet">
        <!-- Flexslider  -->
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/flexslider.css' ?>">
        <!-- Theme style  -->
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/style.css?ss=s' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/style_bg.css' ?>">

        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/colorcodes.css?c=c' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/css/font-awesome.min.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/public/fonts/font-awesome.min.css' ?>">
        
        <!-- Modernizr JS -->
        <script src="<?php echo base_url() . 'webres/public/js/modernizr-2.6.2.min.js' ?>"></script>

        <script src="<?php echo base_url() . 'webres/public/js/jquery.min.js' ?>"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        
*/ ?>

    </head>
