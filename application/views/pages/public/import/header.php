<style>
<?php /*
<!--

.about {
	display: inline-block;
	vertical-align: middle;
	position: relative
}

.about__link {
	font: 700 14px/16px 'Circe', Arial, Helvetica, sans-serif;
	color: #fff;
	letter-spacing: 0px;
	display: inline-block;
	vertical-align: middle;
	padding: 3px 25px 4px 12px;
	-webkit-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
	position: relative
}

.about__link:after {
	content: '';
	position: absolute;
	width: calc(100% - 34px);
	height: 1px;
	bottom: -1px;
	left: 12px;
	background-color: #fff;
	opacity: 0
}

.about__link:hover {
	opacity: 0.7
}

.about__link_active:after {
	opacity: 1
}

.about__dropdown {
	position: absolute;
	top: 18px;
	left: 0;
	width: 100%;
	display: none;
	padding-top: 10px
}

.about__subitem {
	display: block;
	padding: 0 0 0 27px;
	font: 300 12px/27px 'Circe', Arial, Helvetica, sans-serif;
	text-align: left;
	width: 100%;
	color: #F2F2F2;
	-webkit-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease
}

.about__subitem:hover {
	opacity: 0.7
}

.button-about {
<?php /*	width: 14px;
	height: 14px;
	position: relative;
	display: inline-block;
	vertical-align: top;
	margin-top: 4px;
	-webkit-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden 
}

.button-about__line {
	display: block;
	width: 100%;
	height: 1px;
	background-color: rgba(242, 242, 242, 0.5);
	-webkit-transition: all 0.5s ease;
	-o-transition: all 0.5s ease;
	transition: all 0.5s ease
}

.button-about__line:nth-of-type(1),
.button-about__line:nth-of-type(2) {
	margin-bottom: 3px
}

.button-about:after {
	content: '';
	position: absolute;
	width: calc(100% + 10px);
	height: calc(100% + 10px);
	top: -5px;
	left: -5px;
	cursor: pointer
}

.button-about_active {
	-webkit-transform: rotate(90deg);
	-ms-transform: rotate(90deg);
	transform: rotate(90deg)
}

    
-->
*/ ?>
</style> 

<section class="header header_dark" data-header>
		<div class="header__bg" data-header-bg></div>
		<div class="wrapper wrapper_full header__wrapper">
			<div class="header__inner">
				<a data-cursor-hide href="/" class="logo header__logo"> <svg
						class="logo__svg" width="100%" height="100%" viewbox="0 0 234 54"
						version="1.1" xmlns="http://www.w3.org/2000/svg"
						xmlns:xlink="http://www.w3.org/1999/xlink">
						<g id="Canvas" fill="none">
						<g id="logotype">
						<g id="Group">
						<g id="Vector">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M -1.52588e-06 53.6L 53.6 53.6L 53.6 3.05176e-06L -1.52588e-06 3.05176e-06L -1.52588e-06 53.6ZM 47.1 47.1L 30 47.1L 30 27.9L 47.1 8.3L 47.1 47.1ZM 39.7 6.5L 26.8 21.3L 13.9 6.5L 39.7 6.5ZM 6.5 8.3L 23.6 27.9L 23.6 47.1L 6.5 47.1L 6.5 8.3ZM 180.1 3.05176e-06L 180.1 53.6L 233.7 53.6L 233.7 3.05176e-06L 180.1 3.05176e-06L 180.1 3.05176e-06ZM 186.5 47.1L 186.5 8.3L 219.7 47.1L 186.5 47.1ZM 193.9 6.5L 227.1 6.5L 227.1 45L 193.9 6.5ZM 60 53.6L 113.6 53.6L 113.6 3.05176e-06L 60 3.05176e-06L 60 53.6ZM 107.1 6.5L 107.1 47.1L 73.8 47.1L 91.4 27.9L 73.8 6.4L 107.1 6.4L 107.1 6.5ZM 66.5 8.3L 82.7 27.9L 66.5 44.9L 66.5 8.3ZM 120 53.6L 173.6 53.6L 173.6 3.05176e-06L 120 3.05176e-06L 120 53.6ZM 167.1 47.1L 133.9 47.1L 167.1 8.3L 167.1 47.1ZM 126.5 6.5L 159.7 6.5L 126.5 45L 126.5 6.5Z"
							fill="#f7f7f7"></path></g></g></g></g></svg> <svg
						class="logo__svg logo__svg_dark" width="100%" height="100%"
						viewbox="0 0 234 54" version="1.1"
						xmlns="http://www.w3.org/2000/svg"
						xmlns:xlink="http://www.w3.org/1999/xlink">
						<g id="Canvas" fill="none">
						<g id="logotype">
						<g id="Group">
						<g id="Vector">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M -1.52588e-06 53.6L 53.6 53.6L 53.6 3.05176e-06L -1.52588e-06 3.05176e-06L -1.52588e-06 53.6ZM 47.1 47.1L 30 47.1L 30 27.9L 47.1 8.3L 47.1 47.1ZM 39.7 6.5L 26.8 21.3L 13.9 6.5L 39.7 6.5ZM 6.5 8.3L 23.6 27.9L 23.6 47.1L 6.5 47.1L 6.5 8.3ZM 180.1 3.05176e-06L 180.1 53.6L 233.7 53.6L 233.7 3.05176e-06L 180.1 3.05176e-06L 180.1 3.05176e-06ZM 186.5 47.1L 186.5 8.3L 219.7 47.1L 186.5 47.1ZM 193.9 6.5L 227.1 6.5L 227.1 45L 193.9 6.5ZM 60 53.6L 113.6 53.6L 113.6 3.05176e-06L 60 3.05176e-06L 60 53.6ZM 107.1 6.5L 107.1 47.1L 73.8 47.1L 91.4 27.9L 73.8 6.4L 107.1 6.4L 107.1 6.5ZM 66.5 8.3L 82.7 27.9L 66.5 44.9L 66.5 8.3ZM 120 53.6L 173.6 53.6L 173.6 3.05176e-06L 120 3.05176e-06L 120 53.6ZM 167.1 47.1L 133.9 47.1L 167.1 8.3L 167.1 47.1ZM 126.5 6.5L 159.7 6.5L 126.5 45L 126.5 6.5Z"
							fill="#1a1a1a"></path></g></g></g></g></svg>
				</a>
				<div class="header__container">
					<button data-cursor-hide
						class="button-header header__button-header" data-nav-btn>
						<span class="button-header__line"></span> <span
							class="button-header__line"></span>
					</button>
					<div class="menu header__menu" data-cursor-hide>
						
						<div class="projects menu__projects">
							<button class="button-projects projects__button-projects"
								data-projects-btn>
								<span class="button-projects__line"></span> <span
									class="button-projects__line"></span> <span
									class="button-projects__line"></span>
							</button>
							<div class="projects__dropdown" data-projects-dropdown>
								<a href="<?=base_url('our-mission') ?>" class="projects__subitem">Mission </a> 
								<a href="<?=base_url('our-vision') ?>"	class="projects__subitem"> Vision </a> 
							</div>
							<a href="#" class="projects__link "> ABOUT </a>
						</div>
						
						<a href="<?=base_url('our-process') ?>" class="menu__item ">OUR PROCESS </a> 
						<div class="projects menu__projects">
							<button class="button-projects projects__button-projects"
								data-projects-btn>
								<span class="button-projects__line"></span> <span
									class="button-projects__line"></span> <span
									class="button-projects__line"></span>
							</button>
							<div class="projects__dropdown" data-projects-dropdown>
								<a href="<?=base_url('our-projects/commercial') ?>" class="projects__subitem">Commercial </a> 
								<a href="<?=base_url('our-projects/residential') ?>"	class="projects__subitem">Residential </a> 
							</div>
							<a href="<?=base_url('our-projects') ?>" class="projects__link "> PROJECTS </a>
						</div> 
						
						<a href="<?=base_url('contacts-us') ?>" class="menu__item "> CONTACT </a> 
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
	
	
	<!-- menu popu  -->
	<div class="modal" data-modal="navigation">
			<div class="modal__outer">
				<div class="modal__overlay"></div>
				<div class="modal__inner">
					<div class="modal__content">
						<div class="navigation">
							<div class="navigation__bg">
								<span data-split-letters-big="500" data-nav-bg> Menu </span>
							</div>
							<div class="navigation__inner">
								<div class="navigation__column">
									<div class="contact contact_for-nav navigation__contact">
										<div class="contact__inner">
											<div class="contact__list">
												<div class="contact__row">
													<div class="card contact__card">
														<h4 class="h4 card__h4">General Inquiries:</h4>
														<a href="mailto:info@yodezeen.com" class="card__info">
															info@xxxxx.com </a>
													</div>
													<div class="card contact__card">
														<h4 class="h4 card__h4">PR&Collaborations:</h4>
														<a href="mailto:pr@yodezeen.com" class="card__info">
															pr@yyyyy.com </a>
													</div>
													<div class="card contact__card">
														<h4 class="h4 card__h4">Careers4:</h4>
														<a href="mailto:hr@yodezeen.com" class="card__info">
															hr@zzzzz.com </a>
													</div>
												</div>
												<div class="contact__row"></div>
											</div>
											<div class="text contact__text">
												<p>For each project the approach rests on a careful
													understanding of the space, or the site’s.</p>
											</div>
											<button class="button button_white contact__button"
												data-btn-in-nav data-modal-open="contact">
												<span class="button__text"> Send request </span>
												<svg class="button__svg" height="100%" width="100%"
													xmlns="http://www.w3.org/2000/svg">
													<rect class="button__shape" height="100%" width="100%"></rect></svg>
											</button>
										</div>
									</div>
								</div>
								<div class="navigation__column">
									<div class="menu-panel navigation__menu-panel">
										<a href="<?=base_url('our-projects') ?>" class="menu-panel__item"><span> Projects</span></a> 
										<a href="<?=base_url('our-mission') ?>" class="menu-panel__item"> <span>Mission </span>	</a> 
										<a href="<?=base_url('our-vision') ?>" class="menu-panel__item"> <span>Vision </span>	</a> 
										<a href="<?=base_url('contacts-us') ?>" class="menu-panel__item"> <span>Contact </span></a>
										<a href="<?=base_url('our-process') ?>" class="menu-panel__item"> <span> Process </span>	</a> 
										
										
									</div>
									
									<div class="share share_for-panel navigation__share">
										<a href="#" target="_blank"
											class="share__socnetwork"> <span
											class="share__icon socicon socicon-behance"></span>
										</a> <a href="#"
											target="_blank" class="share__socnetwork"> <span
											class="share__icon socicon socicon-facebook"></span> 
										</a> <a href="#"
											target="_blank" class="share__socnetwork"> <span
											class="share__icon socicon socicon-instagram"></span>
										</a> <a href="#" target="_blank"
											class="share__socnetwork"> <span
											class="share__icon socicon socicon-pinterest"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	
	
	
	
	
	
	
	<!-- cursor 11111111 start -->
	<?php  /*?>
	<div class="cursor">
		<div class="cursor__element">
			<div class="cursor__bg cursor__bg_white">
				<svg width="120" height="40" viewbox="0 0 120 40" version="1.1"
					xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink">
					<g data-name="Canvas" fill="none">
					<rect width="120" height="40"></rect>
					<g data-name="cursor white">
					<g data-name="Group">
					<g class="cursor__path cursor__path_right" data-name="back (1)">
					<g data-name="Group">
					<g data-name="Vector">
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						transform="translate(69 24) scale(-1)" fill="#F7F7F7"></path>
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						stroke-width="0.3" stroke-miterlimit="8"
						transform="translate(69 24) scale(-1)" stroke="#F7F7F7"></path></g></g></g>
					<g class="cursor__path cursor__path_left" data-name="back (1)">
					<g data-name="Group">
					<g data-name="Vector">
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						transform="translate(51 16)" fill="#F7F7F7"></path>
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						stroke-width="0.3" stroke-miterlimit="8"
						transform="translate(51 16)" stroke="#F7F7F7"></path></g></g></g></g>
					<g data-name="Ellipse">
					<circle cx="20" cy="20" r="19.5" transform="translate(40 0)"
						stroke="#F7F7F7"></circle></g></g></g></svg>
			</div>
			<div class="cursor__bg cursor__bg_black">
				<svg width="120" height="40" viewbox="0 0 120 40" version="1.1"
					xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink">
					<g data-name="Canvas" fill="none">
					<rect width="120" height="40"></rect>
					<g data-name="cursor black 2">
					<g data-name="Group">
					<g class="cursor__path cursor__path_right" data-name="back (1)">
					<g data-name="Group">
					<g data-name="Vector">
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						transform="translate(69 24) scale(-1)" fill="black"></path>
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.0788221 3.84052L -0.018024 3.72597L 4.44559 -0.0479039C 4.6066 -0.184032 4.85702 -0.184032 5.01802 -0.0479039C 5.10174 0.0228784 5.15 0.121547 5.15 0.226751C 5.15 0.331955 5.10174 0.430624 5.01802 0.501406L 0.880224 3.99982L 5.01604 7.49817C 5.09976 7.56896 5.14802 7.66763 5.14802 7.77283C 5.14802 7.87759 5.10017 7.97586 5.01711 8.04658C 4.93396 8.11906 4.82797 8.15 4.73082 8.15C 4.63081 8.15 4.52635 8.11576 4.44561 8.04751C 4.44561 8.0475 4.4456 8.04749 4.44559 8.04748L -0.0179989 4.27531L -0.0180239 4.27528C -0.101743 4.2045 -0.15 4.10583 -0.15 4.00063C -0.15 3.89543 -0.101743 3.79676 -0.0180241 3.72597L 0.0788221 3.84052Z"
						stroke-width="0.3" stroke-miterlimit="8"
						transform="translate(69 24) scale(-1)" stroke="black"></path></g></g></g>
					<g class="cursor__path cursor__path_left" data-name="back (1)">
					<g data-name="Group">
					<g data-name="Vector">
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.880224 3.99982L 5.01802 0.501406C 5.10174 0.430624 5.15 0.331955 5.15 0.226751C 5.15 0.121547 5.10174 0.0228781 5.01802 -0.0479041C 4.85702 -0.184032 4.6066 -0.184032 4.44559 -0.0479041L -0.0180239 3.72597C -0.101743 3.79676 -0.15 3.89543 -0.15 4.00063C -0.15 4.10583 -0.101743 4.2045 -0.0180239 4.27528L -0.0179988 4.27531L 4.44559 8.04748C 4.4456 8.04749 4.44561 8.0475 4.44561 8.04751C 4.52635 8.11576 4.63081 8.15 4.73082 8.15C 4.82797 8.15 4.93396 8.11906 5.01711 8.04658C 5.10017 7.97586 5.14802 7.87759 5.14802 7.77283C 5.14802 7.66763 5.09976 7.56896 5.01604 7.49817L 0.880224 3.99982Z"
						transform="translate(51 16)" fill="black"></path>
					<path fill-rule="evenodd" clip-rule="evenodd"
						d="M 0.880224 3.99982L 5.01802 0.501406C 5.10174 0.430624 5.15 0.331955 5.15 0.226751C 5.15 0.121547 5.10174 0.0228781 5.01802 -0.0479041C 4.85702 -0.184032 4.6066 -0.184032 4.44559 -0.0479041L -0.0180239 3.72597C -0.101743 3.79676 -0.15 3.89543 -0.15 4.00063C -0.15 4.10583 -0.101743 4.2045 -0.0180239 4.27528L -0.0179988 4.27531L 4.44559 8.04748C 4.4456 8.04749 4.44561 8.0475 4.44561 8.04751C 4.52635 8.11576 4.63081 8.15 4.73082 8.15C 4.82797 8.15 4.93396 8.11906 5.01711 8.04658C 5.10017 7.97586 5.14802 7.87759 5.14802 7.77283C 5.14802 7.66763 5.09976 7.56896 5.01604 7.49817L 0.880224 3.99982Z"
						stroke-width="0.3" stroke-miterlimit="8"
						transform="translate(51 16)" stroke="black"></path></g></g></g></g>
					<g data-name="Ellipse">
					<circle cx="20" cy="20" r="19.5" transform="translate(40 0)"
						stroke="black"></circle></g></g></g></svg>
			</div>
		</div>
	</div>
	*/ ?>
	<!--  cursor 11111111 end -->