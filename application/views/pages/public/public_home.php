<!DOCTYPE html>
<html lang="en">

    <head>
  <meta charset="utf-8">
  <title>ZIYA HR</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url() . 'webres/public/img/favicon.png?a=a'?>" rel="icon">
  <link href="<?php echo base_url() . 'webres/public/img/apple-touch-icon.png?a=a'?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url() . 'webres/public/lib/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  
  <link href="<?php echo base_url() . 'webres/public/lib/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet">
  <link href="<?php echo base_url() . 'webres/public/lib/animate/animate.min.css'?>" rel="stylesheet">
  <link href="<?php echo base_url() . 'webres/public/lib/venobox/venobox.css'?>" rel="stylesheet">
  <link href="<?php echo base_url() . 'webres/public/lib/owlcarousel/assets/owl.carousel.min.css'?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url() .'webres/public/css/style.css'?>" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

        <!-- =======================================================
          Theme Name: TheEvent
          Theme URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
          Author: BootstrapMade.com
          License: https://bootstrapmade.com/license/
        
        ======================================================= -->
    </head>

    <body>

        <!--==========================
          Header
        ============================-->
        <button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>
        <header id="header">
            <div class="container">

                <div id="logo" class="float-left">
                    <!-- Uncomment below if you prefer to use a text logo -->
                    <!-- <h1><a href="#main">C<span>o</span>nf</a></h1>-->
                    <a href="#intro" class="scrollto"><img src="<?php echo base_url() . 'webres/public/img/logo.png?a=a' ?>" alt="" height="200" width="100" title=""></a>
                    
                </div>

                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="menu-active"><a href="#intro">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#speakers">Our Team</a></li>
                        <li><a href="#schedule">Why Us</a></li>
                        <li><a href="#venue">Our office</a></li>
                        <!--<li><a href="#hotels">Our Works</a></li>-->
                        <li><a href="#gallery">Gallery</a></li>
                        <li><a href="#supporters">Clients</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <!--<li class="buy-tickets"><a href="#buy-tickets">Buy Tickets</a></li>-->
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </header><!-- #header -->

        <!--==========================
          Intro Section
        ============================-->
        <section id="intro">
            <div class="intro-container wow fadeIn">
                <h1 class="mb-4 pb-0">The Complete<br><span>Event Management</span> Solution</h1>
                <p class="mb-4 pb-0">Since,2013-with 100+ service advisors, 5000+ staffs and much more</p>
               <?php /* <a href="#" class="venobox play-btn mb-4" data-vbtype="video"
                   data-autoplay="true"></a>*/?>
                <a href="#about" class="about-btn scrollto">About Us</a>
            </div>
        </section>

        <main id="main">

            <!--==========================
              About Section
            ============================-->
            <section id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2>About Us</h2>
                            <p>Ziya HR Solutions is one of the best leading man power supply company in the field of catering and event management. From 2013 ziya actively providing valuable contributions on the catering and event management sector in Kerala and not only in Kerala were we working in Karnataka and Tamilnadu. We are successfully placing more than 15000 per year. We also have more than 20+ most reputed clients were happy to business with us </p><BR>
                            <P>We are not only providing man power to the catering and event management we also assuring the best hospitality management service throw our site management system which includes various executives level such as Service boys , Service advisors, Service supervisors and service Managers. We also providing the best work spice for feel them they were not working they were just living in it.</p><BR>

                        </div>
                        <div class="col-lg-3">
                            <h3>Where</h3>
                            <p>11/552 GROUND FLOOR, MADRAS BULDING, AREEKODE ROAD, KONDOTTY, MALAPPURAM, KERALA, INDIA, 673648</p>
                        </div>

                    </div>
                </div>
            </section>

            <!--==========================
              Speakers Section
            ============================-->
            <section id="speakers" class="wow fadeInUp">
                <div class="container">
                    <div class="section-header">
                        <h2>Our Leaders</h2>
                        <p>Here are some of our Leaders</p>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/anwarsaleem.jpg' ?>" alt="Speaker 1" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html"</a>Anwar Saleem</h3>
                                    <p>Marketing Manager</p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/badusha.jpg' ?>" alt="Speaker 2" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">M.A Badusha.p</a></h3>
                                    <p>Managing Director</p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/fayis.jpg' ?>" alt="Speaker 3" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Fayiz MT</a></h3>
                                    <p>HR Manager </p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/murshid.jpg' ?>" alt="Speaker 4" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Murshid k</a></h3>
                                    <p>Director</p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/salman.jpg' ?>" alt="Speaker 5" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Salman p</a></h3>
                                    <p>Head OF HR Department</p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="speaker">
                                <img src="<?php echo base_url() . 'webres/public/images/ziyamanager/shihab.jpg' ?>" alt="Speaker 6" class="img-fluid">
                                <div class="details">
                                    <h3><a href="speaker-details.html">Shihab P</a></h3>
                                    <p>Service Manager</p>
                                    <div class="social">
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <!--==========================
              Schedule Section
            ============================-->
            <section id="schedule" class="section-with-bg">
                <div class="container wow fadeInUp">
                    <div class="section-header">
                        <h2>OUR SPECIALITY</h2>
                        <p>Here is our event schedule</p>
                    </div>

                    <!--<ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" href="#day-1" role="tab" data-toggle="tab">Day 1</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#day-2" role="tab" data-toggle="tab">Day 2</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#day-3" role="tab" data-toggle="tab">Day 3</a>
                      </li>
                    </ul>
            
                    <h3 class="sub-heading">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius
                      necessitatibus voluptatem quis labore perspiciatis quia.</h3>
            
                    <div class="tab-content row justify-content-center">
            
                    <!-- Schdule Day 1 -->
                    <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">

                        <div class="row schedule-item">
                          <!--<div class="col-md-2"><time>09:30 AM</time></div>-->
                            <div class="col-md-10">
                                <h4>Best hospitality  </h4>

                            </div>
                        </div>
                        <div class="row schedule-item">
                        <!--<div class="col-md-2"><time>09:30 AM</time></div>-->
                            <div class="col-md-10">
                                <h4>Structured management  </h4>

                            </div>
                        </div>
                        <div class="row schedule-item">
                         <!-- <div class="col-md-2"><time>10:00 AM</time></div>-->
                            <div class="col-md-10">
                                <!--<div class="speaker">
                                  <img src="<?php echo base_url() . 'webres/public/img/speakers/1.jpg' ?>" alt="Brenden Legros">
                                </div>-->
                                <h4>7 years of experience </h4>

                            </div>
                        </div>

                        <div class="row schedule-item">
                          <!--<div class="col-md-2"><time>11:00 AM</time></div>-->
                            <div class="col-md-10">
                                <!--<div class="speaker">
                                  <img src="<?php echo base_url() . 'webres/public/img/speakers/2.jpg' ?>" alt="Hubert Hirthe">
                                </div>-->
                                <h4>5000 + part-time Staffs</h4>

                            </div>
                        </div>

                        <div class="row schedule-item">
                         <!-- <div class="col-md-2"><time>12:00 AM</time></div>-->
                            <div class="col-md-10">
                                <!-- <div class="speaker">
                                   <img src="<?php echo base_url() . 'webres/public/img/speakers/3.jpg' ?>" alt="Cole Emmerich">
                                 </div>-->
                                <h4>32 Service Managers</h4>

                            </div>
                        </div>

                        <div class="row schedule-item">
                          <!--<div class="col-md-2"><time>02:00 PM</time></div>-->
                            <div class="col-md-10">
                                <!--<div class="speaker">
                                  <img src="<?php echo base_url() . 'webres/public/img/speakers/4.jpg' ?>" alt="Jack Christiansen">
                                </div>-->
                                <h4>40 Service Supervisors</h4>

                            </div>
                        </div>

                        <div class="row schedule-item">
                         <!-- <div class="col-md-2"><time>03:00 PM</time></div>-->
                            <div class="col-md-10">
                                <!-- <div class="speaker">
                                   <img src="<?php echo base_url() . 'webres/public/img/speakers/5.jpg' ?>" alt="Alejandrin Littel">
                                 </div>-->
                                <h4>100+ Service Advisors</h4>

                            </div>
                        </div>

                        <div class="row schedule-item">
                         <!-- <div class="col-md-2"><time>04:00 PM</time></div>-->
                            <div class="col-md-10">
                                <!--<div class="speaker">
                                  <img src="<?php echo base_url() . 'webres/public/img/speakers/6.jpg' ?>" alt="Willow Trantow">
                                </div>-->
                                <h4>Special theme uniforms</h4>

                            </div>
                        </div>

                    </div>
                    <!-- End Schdule Day 1 -->

                    <!-- Schdule Day 2 -->
                    <!-- <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>10:00 AM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/1.jpg' ?>" alt="Brenden Legros">
                           </div>
                           <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                           <p>Facere provident incidunt quos voluptas.</p>
                         </div>
                       </div>
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>11:00 AM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/2.jpg' ?>" alt="Hubert Hirthe">
                           </div>
                           <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                           <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                         </div>
                       </div>
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>12:00 AM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/3.jpg' ?>" alt="Cole Emmerich">
                           </div>
                           <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                           <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                         </div>
                       </div>
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>02:00 PM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/4.jpg' ?>" alt="Jack Christiansen">
                           </div>
                           <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                           <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                         </div>
                       </div>
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>03:00 PM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/5.jpg' ?>" alt="Alejandrin Littel">
                           </div>
                           <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                           <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                         </div>
                       </div>
           
                       <div class="row schedule-item">
                         <div class="col-md-2"><time>04:00 PM</time></div>
                         <div class="col-md-10">
                           <div class="speaker">
                             <img src="<?php echo base_url() . 'webres/public/img/speakers/6.jpg' ?>" alt="Willow Trantow">
                           </div>
                           <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                           <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                         </div>
                       </div>
           
                     </div>
                    <!-- End Schdule Day 2 -->

                    <!-- Schdule Day 3 -->
                    <!--<div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>10:00 AM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/2.jpg' ?>" alt="Hubert Hirthe">
                          </div>
                          <h4>Et voluptatem iusto dicta nobis. <span>Hubert Hirthe</span></h4>
                          <p>Maiores dignissimos neque qui cum accusantium ut sit sint inventore.</p>
                        </div>
                      </div>
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>11:00 AM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/3.jpg' ?>" alt="Cole Emmerich">
                          </div>
                          <h4>Explicabo et rerum quis et ut ea. <span>Cole Emmerich</span></h4>
                          <p>Veniam accusantium laborum nihil eos eaque accusantium aspernatur.</p>
                        </div>
                      </div>
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>12:00 AM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/1.jpg' ?>" alt="Brenden Legros">
                          </div>
                          <h4>Libero corrupti explicabo itaque. <span>Brenden Legros</span></h4>
                          <p>Facere provident incidunt quos voluptas.</p>
                        </div>
                      </div>
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>02:00 PM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/4.jpg' ?>" alt="Jack Christiansen">
                          </div>
                          <h4>Qui non qui vel amet culpa sequi. <span>Jack Christiansen</span></h4>
                          <p>Nam ex distinctio voluptatem doloremque suscipit iusto.</p>
                        </div>
                      </div>
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>03:00 PM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/5.jpg' ?>" alt="Alejandrin Littel">
                          </div>
                          <h4>Quos ratione neque expedita asperiores. <span>Alejandrin Littel</span></h4>
                          <p>Eligendi quo eveniet est nobis et ad temporibus odio quo.</p>
                        </div>
                      </div>
          
                      <div class="row schedule-item">
                        <div class="col-md-2"><time>04:00 PM</time></div>
                        <div class="col-md-10">
                          <div class="speaker">
                            <img src="<?php echo base_url() . 'webres/public/img/speakers/6.jpg' ?>" alt="Willow Trantow">
                          </div>
                          <h4>Quo qui praesentium nesciunt <span>Willow Trantow</span></h4>
                          <p>Voluptatem et alias dolorum est aut sit enim neque veritatis.</p>
                        </div>
                      </div>
          
                    </div>
                    <!-- End Schdule Day 2 -->

                </div>

                </div>

            </section>

            <!--==========================
              Venue Section
            ============================-->
            <section id="venue" class="wow fadeInUp">

                <div class="container-fluid">

                    <div class="section-header">
                        <h2>Our Office</h2>
                        <p></p>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-lg-6 venue-map">
                           <!-- <div class="mapouter"> width="600" height="500" id="gmap_canvas"
                              <div class="gmap_canvas"> -->
                                <iframe  src="https://maps.google.com/maps?q=kondotty&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            <!--  </div>
                            </div> -->
                            <!-- <style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style>
                         <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        </div>

                        <div class="col-lg-6 venue-info">
                            <div class="row justify-content-center">
                                <div class="col-11 col-lg-8">
                                    <h3>ZIYA HR SOLUTIONS PRIVATE LIMITED</h3>
                                    <p>11/552 GROUND FLOOR, MADRAS BULDING, AREEKODE ROAD, KONDOTTY, MALAPPURAM, KERALA, INDIA, 673648
                                        7510705050       7510704040.</p>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="container-fluid venue-gallery-container">
                    <div class="row no-gutters">

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery1.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery1.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery2.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery2.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery3.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery3.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery4.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery4.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery5.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery5.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery6.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery6.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery7.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery7.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="venue-gallery">
                                <a href="<?php echo base_url() . 'webres/public/images/office venue/venuegallery8.jpg' ?>" class="venobox" data-gall="venue-gallery">
                                    <img src="<?php echo base_url() . 'webres/public/images/office venue/venuegallery8.jpg' ?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <!--==========================
              Hotels Section
            ============================-->
           <!-- <section id="hotels" class="section-with-bg wow fadeInUp">
        
              <div class="container">
                <div class="section-header">
                  <h2>Hotels</h2>
                  <p>Her are some nearby hotels</p>
                </div>
        
                <div class="row">
        
                  <div class="col-lg-4 col-md-6">
                    <div class="hotel">
                      <div class="hotel-img">
                        <img src="<?php echo base_url() . 'webres/public/img/hotels/1.jpg" alt="Hotel 1' ?>" class="img-fluid">
                      </div>
                      <h3><a href="#">Hotel 1</a></h3>
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <p>0.4 Mile from the Venue</p>
                    </div>
                  </div>
        
                  <div class="col-lg-4 col-md-6">
                    <div class="hotel">
                      <div class="hotel-img">
                        <img src="<?php echo base_url() . 'webres/public/img/hotels/2.jpg' ?>" alt="Hotel 2" class="img-fluid">
                      </div>
                      <h3><a href="#">Hotel 2</a></h3>
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-full"></i>
                      </div>
                      <p>0.5 Mile from the Venue</p>
                    </div>
                  </div>
        
                  <div class="col-lg-4 col-md-6">
                    <div class="hotel">
                      <div class="hotel-img">
                        <img src="<?php echo base_url() . 'webres/public/img/hotels/3.jpg" alt="Hotel 3' ?>" class="img-fluid">
                      </div>
                      <h3><a href="#">Hotel 3</a></h3>
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      <p>0.6 Mile from the Venue</p>
                    </div>
                  </div>
        
                </div>
              </div>
        
            </section>-->

            <!--==========================
              Gallery Section
            ============================-->
            <section id="gallery" class="wow fadeInUp">

                <div class="container">
                    <div class="section-header">
                        <h2>Gallery</h2>
                        <p>Check our gallery from the recent events</p>
                    </div>
                </div>

                <div class="owl-carousel gallery-carousel">
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery9.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery9.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery10.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery10.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery3.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery3.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery4.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery4.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery5.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery5.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery6.jpg' ?>"  class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery6.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery7.jpg' ?> class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery7.jpg' ?>" alt=""></a>
                    <a href="<?php echo base_url() . 'webres/public/images/gallery/gallery8.jpg' ?>" class="venobox" data-gall="gallery-carousel"><img src="<?php echo base_url() . 'webres/public/images/gallery/gallery8.jpg' ?>" alt=""></a>
                </div>

            </section>

            <!--==========================
              Sponsors Section
            ============================-->
            <section id="supporters" class="section-with-bg wow fadeInUp">

      <div class="container">
        <div class="section-header">
          <h2>Our Clients</h2>
        </div>

        <div class="row no-gutters supporters-wrap clearfix">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/1.png'?>" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/2.png'?>" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/3.png'?>" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/4.png'?>" class="img-fluid" alt="">
            </div>
          </div>
          
          <!--<div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/5.png'?>" class="img-fluid" alt="">
            </div>
          </div>
        
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/6.png'?>" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/7.png'?>" class="img-fluid" alt="">
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="supporter-logo">
              <img src="<?php echo base_url() . 'webres/public/img/supporters/8.png'?>" class="img-fluid" alt="">
            </div>
          </div>-->

        </div>

      </div>

    </section>


            <!--==========================
              F.A.Q Section
            ============================-->
         <?php /*   <section id="faq" class="wow fadeInUp">

                <div class="container">

                    <div class="section-header">
                        <h2>F.A.Q </h2>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <ul id="faq-list">

                                <li>
                                    <a data-toggle="collapse" class="collapsed" href="#faq1">Non consectetur a erat nam at lectus urna duis? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq1" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-toggle="collapse" href="#faq2" class="collapsed">Feugiat scelerisque varius morbi enim nunc faucibus a pellentesque? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq2" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-toggle="collapse" href="#faq3" class="collapsed">Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq3" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-toggle="collapse" href="#faq4" class="collapsed">Ac odio tempor orci dapibus. Aliquam eleifend mi in nulla? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq4" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-toggle="collapse" href="#faq5" class="collapsed">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq5" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a data-toggle="collapse" href="#faq6" class="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i class="fa fa-minus-circle"></i></a>
                                    <div id="faq6" class="collapse" data-parent="#faq-list">
                                        <p>
                                            Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
                                        </p>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                </div>

            </section>
*/?>
            <!--==========================
              Subscribe Section
            ============================-->
           <?php /* <section id="subscribe">
                <div class="container wow fadeInUp">
                    <div class="section-header">
                        <h2>Newsletter</h2>
                        <p>Rerum numquam illum recusandae quia mollitia consequatur.</p>
                    </div>

                    <form method="POST" action="#">
                        <div class="form-row justify-content-center">
                            <div class="col-auto">
                                <input type="text" class="form-control" placeholder="Enter your Email">
                            </div>
                            <div class="col-auto">
                                <button type="submit">Subscribe</button>
                            </div>
                        </div>
                    </form>

                </div>
            </section> */ ?>

           

            <!--==========================
              Contact Section
            ============================-->
            <section id="contact" class="section-bg wow fadeInUp">

                <div class="container">

                    <div class="section-header">
                        <h2>Contact Us</h2>
                        <p></p>
                    </div>

                    <div class="row contact-info">

                        <div class="col-md-4">
                            <div class="contact-address">
                                <i class="ion-ios-location-outline"></i>
                                <h3>Address</h3>
                                <address><?php echo Cons::ADDRESS; ?>
                                </address>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="contact-phone">
                                <i class="ion-ios-telephone-outline"></i>
                                <h3>Phone Number</h3>
                                <p><?php echo Cons::CONTACTNO; ?>,<?php echo Cons::CONTACTNO2; ?></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="contact-email">
                                <i class="ion-ios-email-outline"></i>
                                <h3>Email</h3>
                                <p><a href="mailto:<?php echo Cons::MAIL; ?>"><?php echo Cons::MAIL; ?></a></p>
                            </div>
                        </div>

                    </div>
<!--
                    <div class="form">
                        <form id="contact_form"  method="POST" class="form " >
                            <div id="sendmessage">Your message has been sent. Thank you!</div>
                            <div id="errormessage"></div>
                            
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>
                                <div class="text-center"><button type="submit">Send Message</button></div>
                            </form>
                    </div>
-->
                </div>
            </section><!-- #contact -->

        </main>


        <!--==========================
          Footer
        ============================-->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-md-6 footer-info">
                            <img src="<?= base_url('webres/public/img/logo.png?a=a')?>" alt="Ziya Hr Solution">
                            <p>Ziya HR Solutions is one of the best leading man power supply company in the field of catering and event management. From 2013 ziya actively providing valuable contributions on the catering and event management sector in Kerala and not only in Kerala were we working in Karnataka and Tamilnadu. We are successfully placing more than 15000 per year. We also have more than 20+ most reputed clients were happy to business with us</p>
                        </div>

                        <!--<div class="col-lg-3 col-md-6 footer-links">
                          <h4>Useful Links</h4>
                          <ul>
                            <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                          </ul>
                        </div>-->

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-contact">
                            <h4>Contact Us</h4>
                            <p>ZIYA HR SOLUTIONS PRIVATE LIMITED


                                11/552 GROUND FLOOR, MADRAS BULDING <br>
                                AREEKODE ROAD, KONDOTTY, MALAPPURAM<br>
                                KERALA, INDIA, 673648 <br>
                                <strong>Phone:</strong> 7510705050, 7510704040<br>
                                <strong>Email:</strong> ziyahrsolution@gmail.com <br>
                            </p>

                            <div class="social-links">
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="copyright">
                    &copy; Copyright <strong>ZIYA HR</strong>. All Rights Reserved
                </div>
                <div class="credits">
                    <!--
                      All the links in the footer should remain intact.
                      You can delete the links only if you purchased the pro version.
                      Licensing information: https://bootstrapmade.com/license/
                      Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=TheEvent
                    -->
                    <span style="color:black">Designed by <a href=""><span style="color:black">BootstrapMade</span></a></span>
                </div>
            </div>
        </footer><!-- #footer -->

        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script type='text/javascript'>

            /* attach a submit handler to the form */
            $("#contact_form").submit(function (event) {

                /* stop form from submitting normally */
                event.preventDefault();

                $.ajax({
                type: 'post',
                        url: '<?= base_url('contact-us') ?>',
                        data: $('#contact_form').serialize(),
                        success: function () {
                        alert('Succesffully sent your request');
                        },
                error: function(er, err, error){
                alert('Can not sent your mail');
                }
                });

            });
        </script>

        <!-- JavaScript Libraries -->
        <script src="<?php echo base_url() . 'webres/public/lib/jquery/jquery.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/jquery/jquery-migrate.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/easing/easing.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/superfish/hoverIntent.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/superfish/superfish.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/wow/wow.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/venobox/venobox.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'webres/public/lib/owlcarousel/owl.carousel.min.js' ?>"></script>

        <!-- Contact Form JavaScript File -->
        <script src="<?php echo base_url() . 'webres/public/contactform/contactform.js' ?>"></script>

        <!-- Template Main Javascript File -->
        <script src="<?php echo base_url() . 'webres/public/js/main.js' ?>"></script>


        
    </body>

</html>
