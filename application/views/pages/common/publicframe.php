<!DOCTYPE html>
<html>
    <?= $import_top ?> 
    
    <body>
       
       <section class="preloader" data-preloader
		style="transform: translateZ(20px);">
		<div class="preloader__wrap">
			<div class="preloader__letters">
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">1</div>
					<div class="card__symbol card__symbol_small">1</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">2</div>
					<div class="card__symbol card__symbol_small">2</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">3</div>
					<div class="card__symbol card__symbol_small">3</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">4</div>
					<div class="card__symbol card__symbol_small">4</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">5</div>
					<div class="card__symbol card__symbol_small">5</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">6</div>
					<div class="card__symbol card__symbol_small">6</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">7</div>
					<div class="card__symbol card__symbol_small">7</div>
				</div>
				<div class="card preloader__card">
					<div class="card__symbol card__symbol_big">8</div>
					<div class="card__symbol card__symbol_small">8</div>
				</div>
			</div>
			<div class="progress preloader__progress">
				<div class="progress__line" data-progress-line></div>
			</div>
		</div>
	</section>
       
       
            <?= $header ?>
        
                <?= $content; ?>
        
            <?= $footer ?>
        
        
        <?= $import_bottom ?>
    </body>
</html>